# FFQ Nutrient scoring

### Aim:

This project aims to convert raw self-reported dietary intake data for DEGS-1 FFQ into mean daily portion of food items and then into nutrient intake per day.

### Pre-requisites:

- [ ] R (version 4.1)
- [ ] survey files (.lsg / .pdf) for FFQ24h and FFQ7d are provided. Input file (.lsg) is suitable for LimeSurvey directly. Note: reference food picture cannot be provided, but have to be issued directly at the Robert Koch Institute.


### Input files

Reference values for nutrient content from German Nutrient Reference Database “Bundeslebensmittelschlüssel” (BLS, version 3.02, Max Rubner-Institut, Karlsruhe, Germany):

- BLS_Scoring.xlsx

References files for naming items:

- FFQ_items.xlsx

Questionnaire results :

- survey-results_FFQ.csv (example file with raw questionnaire data 
  - NOTE: LimeSurvey results have to be exported in the following way:
    - answer format: answer codes
    - question format: question code & question text
    - seperator: " " (space)

### To Do steps:

- [ ] Open R-script: scoring_FFQ.R (was created with R version 4.1.1)
- [ ] If you are **not** using the 4 weeks/28 days version of the FFQ: 
  - [ ] check if answer codes of the frequency questions in limesurvey are as follows:
    - A1 = "never"
    - A2 = "1x a month"
    - A3 = "2-3x a month"
    - A4 = "1-2x a week"
    - A5 = "3-4x a week"
    - A6 = "5-6x a week"
    - A7 = "1x a day"
    - A8 = "2x a day"
    - A9 = "3x a day"
    - A10 = "4-5x a day"
    - A11 = "more than 5x a day"
   - IF NOT: uncomment and edit section "redefine answers" in scoring_FFQ.R 
- [ ] Edit and uncomment parts relevant for longitudinal or cross-sectional data in these sections:
  - "find and curate incorrect mean daily portions"
  - "scoring for items with food subgroups"
  - "calculate sum nutrient values per participant (per timepoint)"
- [ ] Run script


### Output files:

- mean daily portion raw: FFQ_mean_daily_portion_raw.csv
- mean daily portion condensed: FFQ_mean_daily_portion_results.csv
- mean daily portion condensed with nutrient values: FFQ_mean_daily_portion_results_including_nutrients.csv
- computed mean daily nutrient intake: FFQ_nutrients.csv


### Creation of nutrient reference table (BLS_Scoring.xlsx):

**Of Note:** This reference table was created based on the consensus of up to seven scientific staff members to guarantee an appropriate representation of the respective FFQ items.

- column names:
  - ffq_item: item number from FFQ used for matching during scoring
  - food_group: German description of food group according to original publication
  - food_subgroup: German description of specifications declared in the type questions
  - bls: item code(s) from the German Nutrient Reference Database (BLS) corresponding to the FFQ food item or group
  - answer_ffq: LimeSurvey answer code for the respesctive type questions used for matching during scoring	
  - composition: factors used for calculation of nutrient values
  - macro- and micronutrients as presented in *Table 1*


*Table 1: Overview of macro- and micronutrients (in mg/100g for all nutrients and kcal/100g for energy) including English and German term as well as abbreviations used in the reference table.*

|English term	| German term 	| Abbreviations of nutrients in BLS reference table |
|--- | --- | ---|
|energy	| Energie	| GCAL|
|protein	| Eiweiß	| ZE|
|fat	| Fett	| ZF|
|carbohydrates	| Kohlenhydrate	| ZK|
|dietary fiber	| Ballaststoffe	| ZB|
|overall sugar	| Zucker(gesamt)	| KMD|
|cellulose	| Cellulose	| KBC|
|lignin	| Lignin	| KBL|
|water-soluble fiber	| wasserlösliche Ballaststoffe | 	KBW|
|water-insoluble fiber	| wasserunlösliche Ballaststoffe	| KBN|
|tyrosine	| Tyrosin	| ETYR|
|tryptophan	| Tryptophan	| ETRP|
|saturated fatty acids	| gesättigte Fettsäuren	| FS|
|short-chain fatty acids	| kurzkettige Fettsäuren	| FK|
|medium-chain fatty acids	| mittelkettige Fettsäuren	| FM|
|long-chain fatty acids	| langkettige Fettsäuren	| FL|
|Omega-3 fatty acids	| Omega-3-Fettsäuren	| FO3|
|Omega-6 fatty acids	| Omega-6-Fettsäuren	| FO6|


- Step 1: reference values for food groups without further specifications
  - selection of one BLS item in case of clear item description (e.g. FFQ15 "Cornflakes" $\rightarrow$ BLS C515000 "Cornflakes")
  - selection of more BLS items and calculation of average reference values in case of mixed food group (e.g. FFQ2 "sugary soft drinks" -> BLS N39010 "Coke", N310000 "Lemonades", N01900 "Sweetened, lemon flavored iced tea" and P122000 "Malt beer with sugar")
- Step 2: reference values for food groups with type specifications
  - in case of sub-types for milk (FFQ1) and cooked vegetables (FFQ39) (e.g. 3.5% fat milk or canned vegetables): selection of the according BLS items
  - in case of dilution of fruit (FFQ4) and vegetable juice (FFQ5) with water : multiplication of the pure juice nutrient values with the dilution factor  (e.g. dilution with ~25% water -> all nutrient values of juice are multiplied by 0.75)
  - in case of varying fat contents regarding cream cheese (FFQ21), cheese (FFQ22), yogurt & junket (FFQ23) and cold cuts (FFQ31): nutrient values are the sum of the ratio of low-fat products and normal-fat products
  - in case of different ways of preparation of poultry (FFQ27), meat (FFQ30) and fish (FFQ34): nutrient values are composed of the ratio of cooked fish/meat and deep-fried/breaded fish/meat
  - in case of possible addition of sugar to fruit and herbal tea (FFQ7), black and green tea (FFQ8) and coffee (FFQ9): addition of nutrient values of one or more tea spoons of sugar to nutrient values of respective hot drink
  - in case of "other" milk (FFQ1): average nutrient values (from producers) of different plant-based drinks, which were not listed as answer options before, namely:
    - Reisdrink, Evers Naturkost
    - Reisdrink NATUR, Alnatura
    - Bio Reis-Drink granoVita
    - Vitaquell Bio HaferDrink
    - Provamel Bio HaferDrink
    - Alpro Mandeldrink
  - in case of "I don't know" answers, the following "population standards" were assumed:
    - cream cheese (FFQ21), cheese (FFQ22), cold cuts (FFQ31): normal fat content (not low fat)
    - yogurt & junket (FFQ23): mean of normal and low fat versions (based on higher general availability of low fat yogurt and junket compared to low fat cream cheese, cheese or cold cuts)
    - cooked vegetables (FFQ39): mean of all three sources for vegetables (fresh, frozen, tin)

### Details for nutrient scoring (scoring_FFQ.R):

- Step 1: calculation of the mean daily portion (mdp) according to the origincal publication (Haftenberger et al., 2010):
    mdp =  (quantity * frequency of intake)/(28 days)
- Step 2: missing values are identified, excluded or replaced
  - for cross-sectional data:  
    - if frequency given but no amount: replace with population mean
      - milk (FFQ1): mean of normal and low fat milk
      - juice (FFQ4, FFQ5): non-diluted
      - tea and coffee (FFQ7, FFQ8, FFQ9): without sugar
      - cream cheese (FFQ21), cheese (FFQ22) and cold cuts (FFQ31): normal fat content
      - yogurt & junket (FFQ23): mean of normal fat and low fat versions
      - meat or fish (FFQ27, FFQ30, FFQ34): not deep-fried/breaded
      - cooked vegetables (FFQ39): mean of all three vegetable sources (fresh, frozen, tin)
  - for longitudinal data (here 4 time points with 2 baseline-follow up-pairs): 
    - if 1 time point missing: calculate mean of remaining 3 time points
    - if at least 2 time points missing: exclude baseline-follow up-pair
- Step 3: multiplication of mean daily portion with nutrient value from reference nutrient table (BLS_scoring.xlsx)
    - division by 100 because of nutrient value per 100g and division by 1000 if mg needed to be converted to grams
    - final nutrient values for macronutrients in [g] and for micronutrients in [mg]
- Step 4: identify and remove outliers based on calculated energy intake (if desired) 
    - function used: identify_outliers()-function from the package “rstatix” (0.7.0)
